﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HidingSpot : MonoBehaviour
{

    public Color initColor;
    public Color sameColorButDim;

	// Use this for initialization
	void Start ()
    {
        if(GameObject.FindGameObjectWithTag("Player") != null)
        initColor = GameObject.FindGameObjectWithTag("Player").GetComponent<SpriteRenderer>().color;

        sameColorButDim = initColor;

        sameColorButDim.a = 0.5f;

    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    // called when the cube hits the floor
    void OnCollisionEnter2D(Collision2D col)
    {
        Debug.Log("OnCollisionEnter2D");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            //gameManager.CallGameFinished();
            collision.GetComponent<SpriteRenderer>().color = sameColorButDim;
            collision.tag = "PlayerHiding";

        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "PlayerHiding")
        {
            //gameManager.CallGameFinished();
            collision.GetComponent<SpriteRenderer>().color = initColor;
            collision.tag = "Player";

        }
    }
}
