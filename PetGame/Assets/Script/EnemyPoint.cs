﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPoint : MonoBehaviour
{

    public bool didDetectEnemy;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            didDetectEnemy = true;
            collision.GetComponent<EnemyBased>().FlipSpriteOppositeScale(collision.GetComponent<Transform>());

            //FlipSpriteOpposite(collision.transform.Find("VisionCone"));
        }
       
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="other"></param>
    void OnTriggerExit2D(Collider2D other)
    {
        didDetectEnemy = false;
    }

   
}
