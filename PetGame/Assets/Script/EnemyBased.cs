﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBased : MonoBehaviour
{
    public GameManager gameManager;
    public bool didSeePlayer;
    public bool shouldFlipSprite;
    public float speed;
    public EnemyVision enemyVision;




    /// <summary>
    /// 
    /// </summary>
    private void Init()
    {

        gameManager = GameManager.FindObjectOfType<GameManager>();
    }

    // Use this for initialization
    void Start ()
    {
        Init();

    }
	
	// Update is called once per frame
	void Update ()
    {
       

    }



    /// <summary>
    /// 
    /// </summary>
    /// <param name="transforSprite"></param>
    public void FlipSpriteOppositeScale(Transform transformSprite)
    {
        Vector3 tempTransformSpriteScale = transformSprite.localScale;

        //If the scale is positive then put it to negative else if negagtive put it positive
        if (Mathf.Sign(tempTransformSpriteScale.x) >= 1)
            tempTransformSpriteScale.x = -tempTransformSpriteScale.x;
        else
            tempTransformSpriteScale.x = Mathf.Abs(tempTransformSpriteScale.x);

        transformSprite.localScale = tempTransformSpriteScale;

    }

    public void FlipSpriteOpposite(Transform transforSprite)
    {
        transforSprite.GetComponent<SpriteRenderer>().flipX = !transforSprite.GetComponent<SpriteRenderer>().flipX;
    }


}
