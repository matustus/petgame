﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoisePoint : MonoBehaviour
{

   public GameObject[] enemies;
   public float minDistance = 15;
   public GameObject closest;
   public float smooth = 0.5f;

    void OnEnable()
    {
        enemies = GameObject.FindGameObjectsWithTag("Enemy");

        CheckForClosestEnemy();
        closest.GetComponent<EnemyBased>().FlipSpriteOppositeScale(closest.transform);
    }

    /// <summary>
    /// Should give the indication to the player to go back to his original point and 
    /// if possible flip the sprite
    /// </summary>
    private void OnDisable()
    {
        //closest.GetComponent<EnemyBased>().
        //closest.GetComponent<EnemyBased>().FlipSpriteOppositeScale(closest.transform);
    }

    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void CheckForClosestEnemy()
    {
        foreach (GameObject enemy in enemies)
        {
            float dist = Vector3.Distance(transform.position, enemy.transform.position);

            if (dist < minDistance)
            {
                closest = enemy;
                minDistance = dist;
            }
        }
    }
}
