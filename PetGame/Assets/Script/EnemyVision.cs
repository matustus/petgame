﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyVision : MonoBehaviour
{

    public EnemyBased enemyBasedClass;
    public bool isPlayerDetected;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            isPlayerDetected = true;
            enemyBasedClass.didSeePlayer = isPlayerDetected;

        }
    }

   
}
