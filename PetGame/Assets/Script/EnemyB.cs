﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyB : EnemyBased
{
    public float minDistToHearNoise;
    public bool shouldGoBackToOriginalPoint;
    public Transform originalPoint;
    public Transform currentNoisePoint;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (GameObject.Find("NoisePoint") != null)
            CallMoveToNoise();

    }

    public void CallMoveToNoise()
    {
        StartCoroutine(MoveToNoise());
    }

    public bool FindNoisePoint()
    {
        if (GameObject.Find("NoisePoint") != null)
        {
            currentNoisePoint = GameObject.Find("NoisePoint").transform;
            return (Vector3.Distance(this.transform.position, currentNoisePoint.position) > minDistToHearNoise);
        }

        return false;
    }

    public IEnumerator MoveToNoise()
    {
        if (FindNoisePoint())
        {
            //FlipSpriteOppositeScale(this.transform);
            print("Currently moving to noise point!");
            this.transform.position = Vector3.MoveTowards(this.transform.position,
                new Vector3(currentNoisePoint.position.x, this.transform.position.y, this.transform.position.z), speed * Time.deltaTime);
        }

        yield return null;
    }
}
