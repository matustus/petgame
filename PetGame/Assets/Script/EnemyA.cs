﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyA : EnemyBased
{
    public Transform[] pointList;
    public float timeToMove;
    //Could probably implement this later
    //public float waitForAFewSecs;

	// Use this for initialization
	void Start ()
    {
		
	}

    


    // Update is called once per frame
    void Update ()
    {
        if (didSeePlayer)
        {
            print("Player has been caught now restarting level!");
            gameManager.CallRestartLevel();
            StopAllCoroutines();
        }
        else
        {
            CallMoveFromAToBBackToBack();
        }

    }

    public void CallMoveFromAToBBackToBack()
    {
        StartCoroutine(MoveFromAToBBackToBack());
    }

    public IEnumerator MoveFromAToBBackToBack()
    {
        //var pointA = transform.position;
       
        while (true)
        {
            yield return EaseLerpAToB(this.transform, new Vector3(pointList[0].position.x, this.transform.position.y, this.transform.position.z), new Vector3(pointList[1].position.x, this.transform.position.y, this.transform.position.z), timeToMove);
          
            yield return EaseLerpAToB(this.transform, new Vector3(pointList[1].position.x, this.transform.position.y, this.transform.position.z), new Vector3(pointList[0].position.x, this.transform.position.y, this.transform.position.z), timeToMove);
            //yield return new WaitForSeconds(waitForAFewSecs);
        }
    }


    /// <summary>
    /// This function will lerp the transform of this game object to an end position at a certain time
    /// </summary>
    /// <param name="thisTransform"></param>
    /// <param name="startPos"></param>
    /// <param name="endPos"></param>
    /// <param name="time"></param>
    /// <returns></returns>
     public IEnumerator EaseLerpAToB(Transform thisTransform, Vector3 startPos, Vector3 endPos, float time)
    {
        yield return null;
        float i = 0.0f;
        float rate = 1.0f / time;
        while (i < 1.0f)
        {
            //Could Smooth Damp the movement to avoid the sliding brb
            i += Time.deltaTime * rate;
            //The way the smoothStep is set up allow us to have a ease in and ease out for a smoother lerp
            thisTransform.position = Vector3.Lerp(startPos, endPos, Mathf.SmoothStep(0.0f, 1.0f, Mathf.SmoothStep(0.0f, 1.0f, i)));

            yield return null;
        }
    }
}
