﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class  GameManager : MonoBehaviour
{

    public GameObject player;
    public string currentSceneName;
    public float delayBeforeResetingLevel = 1f;
    public float delayBeforeFinishingLevel = 3f;
    public bool shouldShowGameOverText;
    public GameObject gameOverText;
    public GameObject playerFinishedLevelText;


    void Init()
    {
        Scene scene = SceneManager.GetActiveScene();
        currentSceneName = scene.name;

        player = GameObject.FindGameObjectWithTag("Player");
    }


	// Use this for initialization
	void Start ()
    {
        Init();
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(Input.GetKeyDown(KeyCode.R))
        {
            CallRestartLevel();
        }
	}

    /// <summary>
    /// 
    /// </summary>
    public void CallRestartLevel()
    {
        StartCoroutine(RestartLevel());
    }

    public void CallGameFinished()
    {
        StartCoroutine(GameFinished());
    }

    /// <summary>
    /// As title says. You can have a little delay before restarting the level
    /// WIll have a fade or circle ish ending later as polish
    /// </summary>
    /// <returns></returns>
    public IEnumerator RestartLevel()
    {
        if (shouldShowGameOverText && gameOverText != null)
            gameOverText.SetActive(true);

        yield return new WaitForSeconds(delayBeforeResetingLevel);

        SceneManager.LoadScene(currentSceneName);

    }


    public IEnumerator GameFinished()
    {

        if (playerFinishedLevelText != null)
            playerFinishedLevelText.SetActive(true);

        yield return new WaitForSeconds(delayBeforeFinishingLevel);

        print("Player finished Game!");
        player.SetActive(false);
        //SceneManager.LoadScene(currentSceneName);
    }
    
}
