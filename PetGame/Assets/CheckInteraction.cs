﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckInteraction : MonoBehaviour {

	private Transform m_GroundCheck;    // A position marking where to check if the player is grounded.
	const float k_GroundedRadius = .2f; // Radius of the overlap circle to determine if grounded
	private bool m_Grounded;            // Whether or not the player is grounded.
	private Transform m_CeilingCheck;   // A position marking where to check for ceilings
	const float k_CeilingRadius = .01f; // Radius of the overlap circle to determine if the player can stand up
	private Animator m_Anim;            // Reference to the player's animator component.
	private Rigidbody2D m_Rigidbody2D;
	[SerializeField] private LayerMask m_WhatIsGround;

	private void Awake()
        {
            // Setting up references.
            m_GroundCheck = transform.Find("GroundCheck");
            m_CeilingCheck = transform.Find("CeilingCheck");
            m_Anim = GetComponent<Animator>();
            m_Rigidbody2D = GetComponent<Rigidbody2D>();
        }
	
	// Update is called once per frame
	void Update () {
		
	}

	private void FixedUpdate()
        {
            
        }

	//  private void OnTriggerEnter2D(Collision2D theCollision)
    //     {
    //         // Collider2D[] colliders = Physics2D.OverlapCircleAll(m_GroundCheck.position, k_GroundedRadius, m_WhatIsGround);
    //         // for (int i = 0; i < colliders.Length; i++)
    //         // {
    //         //     Debug.Log(colliders[i].gameObject.name);
    //         //     if (colliders[i].gameObject.tag == "fence")
    //         //         climbable = true;
    //         // }
	// 		Debug.Log("why");
    //         if (theCollision.gameObject.name == "Player")
    //         {
    //             hitable = true;
    //             print("hitable" + hitable);
    //         }
            
    //     }

	private void OnCollisionEnter2D(Collision2D col)
	{
		
	}
}
